import React, { Component } from "react";
import { StyleSheet } from "react-native";
import MapView, { Marker } from "react-native-maps";

class CustomMapView extends Component {
  constructor(props) {
    super();

    this.state = {
      markers: props.markers || []
    };
  }

  render() {
    const markerElements = this.state.markers.length
      ? this.state.markers.map((marker, index) => {
          return (
            <Marker
              key={"marker" + index}
              coordinate={marker.latlng}
              title={marker.title}
              description={marker.description}
            />
          );
        })
      : null;

    return (
      <MapView
        style={styles.map}
        mapType="satellite"
        initialRegion={{
          latitude: this.props.latitude,
          longitude: this.props.longitude,
          latitudeDelta: 0.0057625,
          longitudeDelta: 0.00263125
        }}
      >
        {markerElements}
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  }
});

const mapStyle = [
  {
    featureType: "administrative",
    elementType: "geometry",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "administrative.land_parcel",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "administrative.neighborhood",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "road",
    elementType: "labels",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "road",
    elementType: "labels.icon",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "transit",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "labels.text",
    stylers: [
      {
        visibility: "off"
      }
    ]
  }
];

export default CustomMapView;
