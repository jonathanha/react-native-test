import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableHighlight,
  Text
} from "react-native";
import AuthAPIService from "../services/auth-api-service";
import { setStateValue } from "../utils/state-utils";

class LoginForm extends Component {
  constructor(params) {
    super(params);
    this.state = {
      username: "",
      password: "",
      errorMsg: "",
      loading: false
    };
  }

  attemptLogin = () => {
    // Reset state before auth call
    const state = { ...this.state };
    state.loading = true;
    state.errorMsg = "";
    this.setState(state);

    // Do auth
    AuthAPIService.signIn(this.state.username, this.state.password)
      .then(result => {
        setStateValue(this, "loading", false);
        this.props.onLoginSuccess();
      })
      .catch(err => {
        setStateValue(this, "errorMsg", "Sign in failed - please try again");
        setStateValue(this, "loading", false);
      });
  };

  render() {
    const buttonTextStyle = this.state.loading
      ? styles.buttonTextDisabled
      : styles.buttonText;

    return (
      <View>
        <TextInput
          style={styles.input}
          textContentType="username"
          underlineColorAndroid="rgba(0,0,0,0)"
          placeholder="Username"
          placeholderTextColor="#ffffff"
          onChangeText={text => setStateValue(this, "username", text)}
        />
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          textContentType="password"
          underlineColorAndroid="rgba(0,0,0,0)"
          placeholder="Password"
          placeholderTextColor="#ffffff"
          onChangeText={text => setStateValue(this, "password", text)}
        />
        <TouchableHighlight style={styles.button} onPress={this.attemptLogin}>
          <Text style={buttonTextStyle}>Sign In</Text>
        </TouchableHighlight>
        <Text style={styles.errorText}>{this.state.errorMsg}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    fontSize: 18,
    textAlign: "left",
    marginVertical: 10,
    color: "#ffffff",
    backgroundColor: "rgba(255,255,255, 0.2)",
    borderRadius: 5,
    paddingHorizontal: 20,
    width: 250
  },
  button: {
    marginVertical: 10,
    backgroundColor: "#363636",
    borderRadius: 5,
    paddingHorizontal: 20,
    width: 250
  },
  buttonText: {
    fontSize: 18,
    textAlign: "center",
    marginVertical: 10,
    color: "#ffffff"
  },
  buttonTextDisabled: {
    fontSize: 18,
    textAlign: "center",
    marginVertical: 10,
    color: "#999999"
  },
  errorText: {
    color: "#ffaa00",
    fontSize: 18,
    width: 250,
    textAlign: "left",
    marginVertical: 10,
    height: 25
  }
});

export default LoginForm;
