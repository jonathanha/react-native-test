import React from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableHighlight
} from "react-native";
const SitesListView = props => {
  return (
    <ScrollView style={styles.listContainer}>
      {props.sites.map(value => {
        return (
          <TouchableHighlight
            key={value.id}
            style={styles.listItem}
            onPress={() => {
              props.onSelectSite(value);
            }}
          >
            <Text style={styles.listItemText}>{value.name}</Text>
          </TouchableHighlight>
        );
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  listContainer: {
    width: "100%"
  },

  listItem: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 25,
    paddingVertical: 30,
    borderBottomWidth: 2,
    borderBottomColor: "#ffffff"
  },

  listItemText: {
    fontSize: 20,
    color: "#ffffff"
  }
});

export default SitesListView;
