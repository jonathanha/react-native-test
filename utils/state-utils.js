module.exports.setStateValue = (thisRef, propName, value) => {
  const state = { ...thisRef.state };
  state[propName] = value;
  thisRef.setState(state);
};
