import config from "../config/config";
import BaseAPIService from "./base-api-service";

class AuthAPIService extends BaseAPIService {
  static SIGN_IN_ENDPOINT = config.API_SERVICE + "signin";
  static SIGN_OUT_ENDPOINT = config.API_SERVICE + "signout";

  static authUser = "";

  static signIn(username, password) {
    const authData = {
      email: username,
      password: password
    };

    return super.post(this.SIGN_IN_ENDPOINT, authData).then(data => {
      this.authUser = data.user;
      return data;
    });
  }

  static signOut() {
    this.authUser = "";

    return super.get(this.SIGN_OUT_ENDPOINT, false).catch(err => {
      // TODO: log error
    });
  }
}

export default AuthAPIService;
