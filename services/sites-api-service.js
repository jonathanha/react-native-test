import config from "../config/config";
import BaseAPIService from "./base-api-service";

class SitesAPIService extends BaseAPIService {
  static endPoint = config.API_SERVICE + "sites/";
  static list() {
    return super.get(this.endPoint);
  }
}

export default SitesAPIService;
