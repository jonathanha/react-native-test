class BaseAPIService {
  static defaultOptions() {
    return {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      credentials: "include"
    };
  }

  static handleJSONResponse(response) {
    let jsonData;
    try {
      jsonData = response.json();
    } catch (error) {
      jsonData = null;
    }

    if (!response.ok || !jsonData) {
      throw (jsonData && jsonData.error) || "An error occurred";
    } else {
      return jsonData;
    }
  }

  static handleResponse(response) {
    if (!response.ok) {
      throw "An error occurred";
    } else {
      return response.body;
    }
  }

  static get(url, expectJSON = true) {
    const options = this.defaultOptions();
    options.method = "GET";

    return fetch(url, options).then(
      expectJSON ? this.handleJSONResponse : this.handleResponse
    );
  }

  static post(url, data, expectJSON = true) {
    const options = this.defaultOptions();
    options.method = "POST";
    options.body = JSON.stringify(data);

    return fetch(url, options).then(
      expectJSON ? this.handleJSONResponse : this.handleResponse
    );
  }

  static put(url, data, expectJSON = true) {
    const options = this.defaultOptions();
    options.method = "PUT";
    options.body = JSON.stringify(data);

    return fetch(url, options).then(
      expectJSON ? this.handleJSONResponse : this.handleResponse
    );
  }

  static delete(url, expectJSON = true) {
    const options = this.defaultOptions();
    options.method = "DELETE";

    return fetch(url, options).then(
      expectJSON ? this.handleJSONResponse : this.handleResponse
    );
  }
}

export default BaseAPIService;
