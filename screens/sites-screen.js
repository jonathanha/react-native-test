import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { Actions } from "react-native-router-flux";
import SitesAPIService from "../services/sites-api-service";
import SitesListView from "../components/sites-list-view";
import { setStateValue } from "../utils/state-utils";

class SitesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sites: []
    };
  }

  componentWillMount() {
    SitesAPIService.list()
      .then(results => {
        setStateValue(this, "sites", results);
      })
      .catch(err => {
        setStateValue(this, "sites", []);
      });
  }

  onSelectSiteHandler = site => {
    Actions.push("site-details", { site: site, title: site.name });
  };

  render() {
    return (
      <View style={styles.container}>
        <SitesListView
          sites={this.state.sites}
          onSelectSite={this.onSelectSiteHandler}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#607d8b",
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "#ffffff"
  }
});

export default SitesScreen;
