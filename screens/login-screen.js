import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";
import { Actions } from "react-native-router-flux";
import LoginForm from "../components/login-form";

class LoginScreen extends Component {
  onLoginSuccessHandler = () => {
    Actions.replace("sites");
  };

  render() {
    return (
      <View style={styles.container}>
        <Image source={require("../assets/logo.png")} style={styles.logo} />
        <LoginForm onLoginSuccess={this.onLoginSuccessHandler} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#607d8b",
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    fontSize: 20,
    color: "#ffffff"
  },
  logo: {
    width: 150,
    height: 150,
    margin: 20,
    marginBottom: 75
  }
});

export default LoginScreen;
