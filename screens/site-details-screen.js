import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import CustomMapView from "../components/custom-map-view";

class SiteDetailsScreen extends Component {
  constructor(props) {
    super();

    const site = props.site;
    let latitude = 0;
    let longitude = 0;
    const markers = [];

    if (
      typeof site.location === "object" &&
      site.location.coordinates instanceof Array
    ) {
      longitude = site.location.coordinates[0];
      latitude = site.location.coordinates[1];

      markers.push({
        latlng: { latitude: latitude, longitude: longitude },
        title: site.name,
        description: ""
      });
    }

    this.state = {
      latitude: latitude,
      longitude: longitude,
      markers: markers
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <CustomMapView
          longitude={this.state.longitude}
          latitude={this.state.latitude}
          markers={this.state.markers}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    backgroundColor: "#607d8b",
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "#ffffff"
  }
});

export default SiteDetailsScreen;
