/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import SitesScreen from "./screens/sites-screen";
import SiteDetailsScreen from "./screens/site-details-screen";
import LoginScreen from "./screens/login-screen";
import { Router, Scene, Stack, Actions } from "react-native-router-flux";
import AuthAPIService from "./services/auth-api-service";

type Props = {};
export default class App extends Component<Props> {
  onLogOut = () => {
    AuthAPIService.signOut();
    Actions.replace("login");
  };

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene
            key="login"
            component={LoginScreen}
            hideNavBar={true}
            initial
          />
          <Scene
            key="sites"
            component={SitesScreen}
            title="Sites"
            rightButtonImage={require("./assets/lock.png")}
            onRight={this.onLogOut}
          />
          <Scene key="site-details" component={SiteDetailsScreen} />
        </Scene>
      </Router>
    );
  }
}
